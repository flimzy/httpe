package httpe

import "net/http"

type handlerError struct{ error }

type Handler = http.Handler

type HandlerFunc = http.HandlerFunc

type HandlerWithError interface {
	ServeHTTPWithError(http.ResponseWriter, *http.Request) error
}

type HandlerWithErrorFunc func(http.ResponseWriter, *http.Request) error

func (f HandlerWithErrorFunc) ServeHTTPWithError(w http.ResponseWriter, r *http.Request) error {
	return f(w, r)
}

func (f HandlerWithErrorFunc) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if err := f(w, r); err != nil {
		panic(handlerError{err})
	}
}

func ToHandler(h HandlerWithError) Handler {
	if h, ok := h.(Handler); ok {
		return h
	}
	return HandlerWithErrorFunc(h.ServeHTTPWithError)
}

func ToHandlerWithError(h Handler) HandlerWithError {
	if hwe, ok := h.(HandlerWithError); ok {
		return hwe
	}
	return HandlerWithErrorFunc(func(w http.ResponseWriter, r *http.Request) (err error) {
		defer func() {
			if r := recover(); r != nil {
				if herr, ok := r.(handlerError); ok {
					err = herr.error
				} else {
					panic(r)
				}
			}
		}()
		h.ServeHTTP(w, r)
		return nil
	})
}

func ToMiddleware(mw func(HandlerWithError) HandlerWithError) func(Handler) Handler {
	return func(next Handler) Handler {
		return ToHandler(mw(ToHandlerWithError(next)))
	}
}

func ToMiddlewareWithError(mw func(Handler) Handler) func(HandlerWithError) HandlerWithError {
	return func(next HandlerWithError) HandlerWithError {
		return ToHandlerWithError(mw(ToHandler(next)))
	}
}
